#!/usr/bin/env bash

rm -rf ../bin
mkdir ../bin
cp ../config/* ../bin/

rm -rf ./dist ./build

# git clone https://github.com/TON-Pool/miner.git
pyinstaller --clean --onefile --add-data "hash_solver.cl:." --add-data "sha256.cl:." --name miner-linux miner.py
cp ./dist/miner-linux ../bin/miner-linux

# ./dist/miner-linux https://next.ton-pool.com EQB6UzwFx-gZTIZmJmiFWZ7_qTIZ9RwBaR1_2IPtKR4UuAoJ

# ../bin/miner-linux https://next.ton-pool.com EQB6UzwFx-gZTIZmJmiFWZ7_qTIZ9RwBaR1_2IPtKR4UuAoJ
